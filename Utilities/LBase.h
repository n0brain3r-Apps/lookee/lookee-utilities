//
//  LBase.h
//  Writeability
//
//  Created by Ryan on 6/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#ifdef __cplusplus
extern "C" {
#endif

char * LBase58Encode(const unsigned char *encode, unsigned long length, unsigned long *encodedLength);
unsigned char * LBase58Decode(const char *decode, unsigned long length, unsigned long *decodedLength);

#ifdef __cplusplus
}
#endif
