//
//  LBitmap.c
//  Writeability
//
//  Created by Ryan on 1/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//



#include "edtaa3func.h"

#include <string.h>
#include <stdlib.h>

#include <stdio.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Macros
#pragma mark -
//*********************************************************************************************************************//




#ifndef MIN
#   define MIN(A, B) ({ __typeof__(A) __A = (A); __typeof__(B) __B = (B); __A<__B?__A:__B; })
#endif

#ifndef MAX
#   define MAX(A, B) ({ __typeof__(A) __A = (A); __typeof__(B) __B = (B); __A<__B?__B:__A; })
#endif




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LBitmap
#pragma mark -
//*********************************************************************************************************************//



#pragma mark - Function Declarations
//*********************************************************************************************************************//

static float LBitmapPixelInterpolate
(
 float factor,
 float component0,
 float component1,
 float component2,
 float component3
);


#pragma mark - Public Functions
//*********************************************************************************************************************//

void LBitmapConvertFromBytes
(
 const void     *bytes,
 unsigned int   width,
 unsigned int   height,
 void           *bitmap
 ) {
    for (unsigned int row = 0; row < height; ++ row) {
        for (unsigned int column = 0; column < width; ++ column) {
            ((double *)bitmap)[row * width + column] = (((unsigned char *)bytes)[row * width + column] /255.);
        }
    }
}

void LBitmapConvertToBytes
(
 const void     *bitmap,
 unsigned int   width,
 unsigned int   height,
 void           *bytes
 ) {
    for (unsigned int row = 0; row < height; ++ row) {
        for (unsigned int column = 0; column < width; ++ column) {
            ((unsigned char *)bytes)[row * width + column] = (255* (1- ((double *)bitmap)[row * width + column]));
        }
    }
}

void LBitmapDistanceField(const void *data, unsigned int width, unsigned int height, void *map) {
    short   *distx      = (short *  )malloc(width * height * sizeof(short));
    short   *disty      = (short *  )malloc(width * height * sizeof(short));
    double  *gx         = (double * )calloc(width * height, sizeof(double));
    double  *gy         = (double * )calloc(width * height, sizeof(double));
    double  *outside    = (double * )calloc(width * height, sizeof(double));
    double  *inside     = (double * )calloc(width * height, sizeof(double));

    computegradient(data, height, width, gx, gy);
    edtaa3(data, gx, gy, width, height, distx, disty, outside);

    for (unsigned int index = 0; index < width * height; ++ index) {
        if (outside[index] < 0.) {
            outside[index] = 0.;
        }
    }

    memset(gx, 0, width * height * sizeof(double));
    memset(gy, 0, width * height * sizeof(double));

    for (unsigned int index = 0; index < width * height; ++ index) {
        ((double *)map)[index] = (1- ((double *)data)[index]);
    }

    computegradient(map, height, width, gx, gy);
    edtaa3(map, gx, gy, width, height, distx, disty, inside);

    for (unsigned int index = 0; index < width * height; ++ index) {
        if (inside[index] < 0) {
            inside[index] = 0.;
        }
    }

    double vmin = +INFINITY;

    for (unsigned int index = 0; index < width * height; ++ index) {
        outside[index] -= inside[index];

        if (outside[index] < vmin) {
            vmin = outside[index];
        }
    }

    vmin = fabs(vmin);

    for (unsigned int index = 0; index < width * height; ++ index) {
        double v = outside[index];

        if (v < -vmin) {
            outside[index] = -vmin;
        } else if (v > +vmin) {
            outside[index] = +vmin;
        }

        ((double *)map)[index] = (outside[index] + vmin) / (2* vmin);
    }

    free(distx);
    free(disty);
    free(gx);
    free(gy);
    free(outside);
    free(inside);
}

void LBitmapResize
(
 const void     *source,
 unsigned int   sourceWidth,
 unsigned int   sourceHeight,
 unsigned int   destinationWidth,
 unsigned int   destinationHeight,
 void           *destination
 ) {
    if (sourceWidth == destinationWidth && sourceHeight == destinationHeight) {
        memcpy(destination, source, sourceWidth * sourceHeight * sizeof(double));
    } else {
        float xScale = (sourceWidth /(float) destinationWidth);
        float yScale = (sourceHeight /(float) destinationHeight);

        for (unsigned int row = 0; row < destinationHeight; ++ row) {
            for (unsigned int column = 0; column < destinationWidth; ++ column) {
                int sourceColumn    = (int)(column * xScale);
                int sourceRow       = (int)(row * yScale);

                int column0 = MIN(MAX(0, sourceColumn -1), sourceWidth -1);
                int column1 = MIN(MAX(0, sourceColumn   ), sourceWidth -1);
                int column2 = MIN(MAX(0, sourceColumn +1), sourceWidth -1);
                int column3 = MIN(MAX(0, sourceColumn +2), sourceWidth -1);
                int row0    = MIN(MAX(0, sourceRow -1)   , sourceHeight -1);
                int row1    = MIN(MAX(0, sourceRow   )   , sourceHeight -1);
                int row2    = MIN(MAX(0, sourceRow +1)   , sourceHeight -1);
                int row3    = MIN(MAX(0, sourceRow +2)   , sourceHeight -1);

                ((double *)destination)[row * destinationWidth + column] =
                LBitmapPixelInterpolate
                (row /(float) destinationHeight,
                 LBitmapPixelInterpolate(column /(float) destinationWidth,
                                         ((double *)source)[row0 * sourceWidth + column0],
                                         ((double *)source)[row0 * sourceWidth + column1],
                                         ((double *)source)[row0 * sourceWidth + column2],
                                         ((double *)source)[row0 * sourceWidth + column3]),
                 LBitmapPixelInterpolate(column /(float) destinationWidth,
                                         ((double *)source)[row1 * sourceWidth + column0],
                                         ((double *)source)[row1 * sourceWidth + column1],
                                         ((double *)source)[row1 * sourceWidth + column2],
                                         ((double *)source)[row1 * sourceWidth + column3]),
                 LBitmapPixelInterpolate(column /(float) destinationWidth,
                                         ((double *)source)[row2 * sourceWidth + column0],
                                         ((double *)source)[row2 * sourceWidth + column1],
                                         ((double *)source)[row2 * sourceWidth + column2],
                                         ((double *)source)[row2 * sourceWidth + column3]),
                 LBitmapPixelInterpolate(column /(float) destinationWidth,
                                         ((double *)source)[row3 * sourceWidth + column0],
                                         ((double *)source)[row3 * sourceWidth + column1],
                                         ((double *)source)[row3 * sourceWidth + column2],
                                         ((double *)source)[row3 * sourceWidth + column3]));
            }
        }
    }

}


#pragma mark - Protected Functions
//*********************************************************************************************************************//

static inline float LBitmapPixelFactor(float x) {
    const float B = 1/3.;
    const float C = 1/3.;

    x = fabs(x);

    if (x < 1) {
        return (  ( 12 -  9 * B - 6 * C) * x * x * x
                + (-18 + 12 * B + 6 * C) * x * x
                + (  6 -  2 * B)) /6;
    } else if (x < 2) {
        return (  (     -B -  6 * C) * x * x * x
                + (  6 * B + 30 * C) * x * x
                + (-12 * B - 48 * C) * x
                + (  8 * B + 24 * C)) /6;
    }

    return 0;
}

float LBitmapPixelInterpolate
(
 float factor,
 float component0,
 float component1,
 float component2,
 float component3
 ) {
    float factor0 = LBitmapPixelFactor(factor - 1);
    float factor1 = LBitmapPixelFactor(factor    );
    float factor2 = LBitmapPixelFactor(factor + 1);
    float factor3 = LBitmapPixelFactor(factor + 2);

    float interpolation = (factor0 * component0 +
                           factor1 * component1 +
                           factor2 * component2 +
                           factor3 * component3);

    return MIN(MAX(interpolation, 0.), 1.);
}