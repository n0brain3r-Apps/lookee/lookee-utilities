//
//  LBitmap.h
//  Writeability
//
//  Created by Ryan on 1/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//



#ifdef __cplusplus
extern "C" {
#endif


void LBitmapConvertFromBytes
(
 const void     *bytes,
 unsigned int   width,
 unsigned int   height,
 void           *bitmap
 );

void LBitmapConvertToBytes
(
 const void     *bitmap,
 unsigned int   width,
 unsigned int   height,
 void           *bytes
 );

void LBitmapDistanceField
(
 const void     *data,
 unsigned int   width,
 unsigned int   height,
 void           *map
 );

void LBitmapResize
(
 const void     *source,
 unsigned int   sourceWidth,
 unsigned int   sourceHeight,
 unsigned int   destinationWidth,
 unsigned int   destinationHeight,
 void           *destination
 );


#ifdef __cplusplus
}
#endif
