//
//  LCollectionViewFlowLayout.m
//  Writeability
//
//  Created by Ryan on 8/21/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LCollectionViewFlowLayout.h"



//*********************************************************************************************************************//
#pragma mark -
#pragma mark WCollectionViewFlowLayout
#pragma mark -
//*********************************************************************************************************************//



@implementation LCollectionViewFlowLayout

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
	NSArray			*attributes		= [super layoutAttributesForElementsInRect:rect];
	NSMutableArray	*newAttributes	= [NSMutableArray arrayWithCapacity:attributes.count];

	for (UICollectionViewLayoutAttributes *attribute in attributes) {
		if ((attribute.frame.origin.x + attribute.frame.size.width <= self.collectionViewContentSize.width) &&
			(attribute.frame.origin.y + attribute.frame.size.height <= self.collectionViewContentSize.height)) {
			[newAttributes addObject:attribute];
		}
	}

	return newAttributes;
}

- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath
{
	return nil;
}

- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath
{
	return nil;
}

@end
