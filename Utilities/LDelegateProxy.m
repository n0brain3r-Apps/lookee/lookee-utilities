//
//  LProxy.m
//  Writeability
//
//  Created by Ryan on 12/10/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LDelegateProxy.h"

#import "LMacros.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LProxy
#pragma mark -
//*********************************************************************************************************************//




@implementation LDelegateProxy
{
    id <LProxyDelegate> __weak __delegate;
}

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init __ILLEGALMETHOD__;

- (instancetype)initWithDelegate:(id <LProxyDelegate>)delegate
{
    if ((self = [super init])) {
        __delegate = delegate;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (BOOL)respondsToSelector:(SEL)selector
{
    return ([__delegate respondsToSelector:selector] ||
            [[__delegate receiver] respondsToSelector:selector]);
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    if ([__delegate respondsToSelector:invocation.selector]) {
        [invocation invokeWithTarget:__delegate];
    }

    if ([[__delegate receiver] respondsToSelector:invocation.selector]) {
        [invocation invokeWithTarget:[__delegate receiver]];
    }
}

@end
