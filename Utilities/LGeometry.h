//
//  LGeometry.h
//  Writeability
//
//  Created by Ryan on 12/1/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LMacros.h"

#import <UIKit/UIImage.h>


#ifdef __cplusplus
extern "C" {
#endif



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGeometry
#pragma mark -
//*********************************************************************************************************************//


#pragma mark - CGAffineTransform Additions
//*********************************************************************************************************************//

static inline UIImageOrientation UIImageOrientationForTransform(CGAffineTransform transform) {
    UIImageOrientation orientation;

    if (!IsNegative(transform.a) && !IsNegative(transform.d)) {
        orientation = UIImageOrientationUp;
    } else if (!IsNegative(transform.a) && IsNegative(transform.d)) {
        orientation = UIImageOrientationDownMirrored;
    } else if (IsNegative(transform.a) && !IsNegative(transform.d)) {
        orientation = UIImageOrientationUpMirrored;
    } else {
        orientation = UIImageOrientationDown;
    }

    return orientation;
}


#pragma mark - CGPoint Additions
//*********************************************************************************************************************//

static inline CGPoint CGPointNull() {
    static CGPoint nullPoint = { NAN, NAN };

    return nullPoint;
}

static inline BOOL CGPointIsNull(CGPoint point) {
    return CGPointEqualToPoint(point, CGPointNull());
}

static inline BOOL CGPointIsOrigin(CGPoint point) {
    return CGPointEqualToPoint(point, CGPointZero);
}

static inline CGPoint CGPointScale(CGPoint point, CGFloat scale) {
    return ((CGPoint) {
        .x = (point.x * scale),
        .y = (point.y * scale)
    });
}


#pragma mark - CGSize Additions
//*********************************************************************************************************************//

static inline BOOL CGSizeIsZero(CGSize size) {
    return CGSizeEqualToSize(size, CGSizeZero);
}

static inline CGSize CGSizeScale(CGSize size, CGFloat scale) {
    return ((CGSize) {
        .width  = (size.width  * scale),
        .height = (size.height * scale)
    });
}

static inline CGSize CGSizeClampToScalar(CGSize size, CGFloat scalar) {
    if (isgreater(size.width , scalar) ||
        isgreater(size.height, scalar)) {
        if (isgreater(size.width, size.height)) {
            float xScale = (scalar /  size.width);

            size.width  = scalar;
            size.height = (size.height * xScale);
        } else {
            float yScale = (scalar / size.height);

            size.width  = (size.width  * yScale);
            size.height = scalar;
        }
    }

    return size;
}


#pragma mark - CGRect Additions
//*********************************************************************************************************************//

static inline CGPoint CGRectGetTopLeftPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMinX(rect),
        .y = CGRectGetMinY(rect)
    });
}

static inline CGPoint CGRectGetTopRightPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMaxX(rect),
        .y = CGRectGetMinY(rect)
    });
}

static inline CGPoint CGRectGetBottomRightPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMaxX(rect),
        .y = CGRectGetMaxY(rect)
    });
}

static inline CGPoint CGRectGetBottomLeftPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMinX(rect),
        .y = CGRectGetMaxY(rect)
    });
}

static inline CGPoint CGRectGetCenterPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMidX(rect),
        .y = CGRectGetMidY(rect)
    });
}

static inline CGPoint CGRectGetTopCenterPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMidX(rect),
        .y = CGRectGetMinY(rect)
    });
}

static inline CGPoint CGRectGetRightCenterPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMaxX(rect),
        .y = CGRectGetMidY(rect)
    });
}

static inline CGPoint CGRectGetBottomCenterPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMidX(rect),
        .y = CGRectGetMaxY(rect)
    });
}

static inline CGPoint CGRectGetLeftCenterPoint(CGRect rect) {
    return ((CGPoint) {
        .x = CGRectGetMinX(rect),
        .y = CGRectGetMidY(rect)
    });
}

static inline CGRect CGRectScale(CGRect rect, CGFloat scale) {
    return ((CGRect) {
        .origin = CGPointScale(rect.origin, scale),
        .size   = CGSizeScale(rect.size, scale)
    });
}

static inline CGRect CGRectUnionPoint(CGRect rect, CGPoint point) {
    return CGRectUnion(rect, ((CGRect) { point, { 0 } }));
}

static inline CGRect CGRectFromPoints(CGPoint from, CGPoint to) {
    return ((CGRect) {
        .origin = {
            .x = fminf(from.x, to.x),
            .y = fminf(from.y, to.y)
        },
        .size   = {
            .width  = fabsf(from.x - to.x),
            .height = fabsf(from.y - to.y)
        }
    });
}

static inline CGRect CGRectClampToRect(CGRect rect, CGRect clamp) {
    CGFloat ratio = (rect.size.width / rect.size.height);
    CGFloat xLimit = (clamp.size.width - rect.size.width);
    CGFloat yLimit = (clamp.size.height - rect.size.height);

    if (isgreater(rect.size.width, rect.size.height)) {
        if (isgreater(rect.size.width, clamp.size.width)) {
            rect.size.width  = (clamp.size.width);
            rect.size.height = (clamp.size.width / ratio);
        }
    } else if (isgreater(rect.size.height, clamp.size.height)) {
        rect.size.width  = (clamp.size.height * ratio);
        rect.size.height = (clamp.size.height);
    }

    if (isgreater(rect.origin.x, xLimit)) {
        rect.origin.x = fmaxf(clamp.origin.x, xLimit);
    } else if (isless(rect.origin.x, clamp.origin.x)) {
        rect.origin.x = clamp.origin.x;
    }

    if (isgreater(rect.origin.y, yLimit)) {
        rect.origin.y = fmaxf(clamp.origin.y, yLimit);
    } else if (isless(rect.origin.y, clamp.origin.y)) {
        rect.origin.y = clamp.origin.y;
    }

    return rect;
}

static inline void CGRectMinusRect
(
 CGRect rect,
 CGRect minus,
 CGRect *top,
 CGRect *right,
 CGRect *bottom,
 CGRect *left
 ) {
    if (CGRectIntersectsRect(rect, minus)) {
        CGRect intersection = CGRectIntersection(rect, minus);

        if (top) {
            CGRectDivide(rect, top, &rect, CGRectGetMinY(rect) - CGRectGetMinY(intersection), CGRectMinYEdge);
        }

        if (right) {
            CGRectDivide(rect, right, &rect, CGRectGetMaxX(intersection) - CGRectGetMaxX(rect), CGRectMaxXEdge);
        }

        if (bottom) {
            CGRectDivide(rect, bottom, &rect, CGRectGetMaxY(intersection) - CGRectGetMaxY(rect), CGRectMaxYEdge);
        }

        if (left) {
            CGRectDivide(rect, left, &rect, CGRectGetMinX(rect) - CGRectGetMinX(intersection), CGRectMinXEdge);
        }

        DBGAssert(CGRectEqualToRect(minus, intersection));
    }
}

    
#ifdef __cplusplus
}
#endif
