//
//  LGroupProxy.h
//  Writeability
//
//  Created by Ryan on 8/15/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LProxy.h"


@interface LGroupProxy : LProxy

- (void)$addObject:(id)object;
- (void)$removeObject:(id)object;

- (NSUInteger)$count;

@end
