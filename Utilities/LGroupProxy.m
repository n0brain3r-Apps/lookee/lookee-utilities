//
//  LGroupProxy.m
//  Writeability
//
//  Created by Ryan on 8/15/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//

// TODO: Cache object instance for faster forwarding


#import "LGroupProxy.h"

#import "LMacros.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LGroupProxy
#pragma mark -
//*********************************************************************************************************************//





@implementation LGroupProxy

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

+ (id)proxyWithObject:(id)object __ILLEGALMETHOD__;

+ (id)proxy
{
    LGroupProxy *instance = [super proxy];
    {
        instance->object = [NSPointerArray weakObjectsPointerArray];
    }

    return instance;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (BOOL)respondsToSelector:(SEL)selector
{
    for (id instance in object) {
        if ([instance respondsToSelector:selector]) {
            return YES;
        }
    }

    return [super respondsToSelector:selector];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector
{
    NSMethodSignature *signature;

    $for (id instance in object) {
        if ((signature = [instance methodSignatureForSelector:selector])) {
            break;
        }
    } else {
        signature = [super methodSignatureForSelector:selector];
    }

    return signature;
}

- (id)forwardingTargetForSelector:(SEL)selector
{
    return nil;
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    NSUInteger index = 0;

    for (id instance in [object copy]) {
        if (!instance) {
            [object removePointerAtIndex:index];
            continue;
        }

        if ([instance respondsToSelector:[invocation selector]]) {
            [invocation invokeWithTarget:instance];
        }

        (index ++);
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)$addObject:(id)instance
{
    DBGParameterAssert(instance != nil);

    [object addPointer:Voidify(instance)];
}

- (void)$removeObject:(id)instance
{
    DBGParameterAssert(instance != nil);

    NSUInteger index = 0;

    for (id storedInstance in [object copy]) {
        if ([storedInstance isEqual:instance]) {
            [object removePointerAtIndex:index];
            break;
        }

        (index ++);
    }
}

- (NSUInteger)$count
{
    NSUInteger count = 0;

    for (id storedInstance in [object copy]) {
        if (!storedInstance) {
            [object removePointerAtIndex:count];
            continue;
        }

        (count ++);
    }

    return count;
}

@end
