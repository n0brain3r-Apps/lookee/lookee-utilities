//
//  LKVO.h
//  Writeability
//
//  Created by Ryan on 11/12/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@protocol LKVOProtocol <NSObject>
@required

@property (nonatomic, readwrite , assign, getter = isEnabled) BOOL enabled;
@property (nonatomic, readonly  , assign, getter = isValid  ) BOOL valid;


- (void)remove;

@end


@interface LKVONotification : NSObject

@property (nonatomic, readonly, copy    ) NSString          *keyPath;
@property (nonatomic, readonly, assign  ) NSKeyValueChange  kind;
@property (nonatomic, readonly, strong  ) id                oldValue;
@property (nonatomic, readonly, weak    ) id                newValue;
@property (nonatomic, readonly, strong  ) NSIndexSet        *indices;
@property (nonatomic, readonly, assign  ) BOOL              isPrior;

@end


@interface LKVO : NSObject

+ (instancetype)allObservers;


- (id <LKVOProtocol>)
addObserver :(id                                        )observer
ofTarget    :(id                                        )target
forKeyPath  :(id <NSFastEnumeration>                    )keyPath
options     :(NSKeyValueObservingOptions                )options
block       :(void(^)(__weak id         observer,
                      __weak id         target  ,
                      LKVONotification  *notification)  )block;

- (id <LKVOProtocol>)
addObserver :(id                        )observer
ofTarget    :(id                        )target
forKeyPath  :(id <NSFastEnumeration>    )keyPath
selector    :(SEL                       )selector
userInfo    :(id                        )userInfo
options     :(NSKeyValueObservingOptions)options;

- (void)
removeObserver  :(id                    )observer
ofTarget        :(id                    )target
forKeyPath      :(id <NSFastEnumeration>)keyPath
selector        :(SEL                   )selector;

- (void)removeObservation:(id <LKVOProtocol>)observation;

@end
