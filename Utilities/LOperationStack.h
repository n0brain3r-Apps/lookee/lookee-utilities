//
//  LOperationStack.h
//  Writeability
//
//  Created by Ryan on 11/12/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface LOperationStack : NSObject

@property (nonatomic, readwrite , copy  ) NSString              *name;
@property (atomic   , readonly  , strong) NSArray               *operations;

@property (atomic   , readonly  , assign) NSUInteger            operationCount;
@property (nonatomic, readwrite , assign) NSInteger             maxConcurrentOperationCount;

@property (atomic   , readwrite , assign, getter = isSuspended) BOOL suspended;


- (void)addOperation:(NSOperation *)operation;
- (void)addOperationWithBlock:(void (^)(void))block;
- (void)addOperations:(NSArray *)operations waitUntilFinished:(BOOL)wait;
- (void)addOperationAtBottomOfStack:(NSOperation *)operation;

- (void)cancelAllOperations;

- (void)waitUntilAllOperationsAreFinished;

@end
