//
//  LTimer.m
//  Writeability
//
//  Created by Ryan on 11/14/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LTimer.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LTimer
#pragma mark -
//*********************************************************************************************************************//




@interface LTimer ()

@property (nonatomic, readwrite , assign) BOOL              isRunning;

@property (nonatomic, readwrite , strong) NSTimer           *NSTimer;

@property (nonatomic, readonly  , assign) NSTimeInterval    interval;
@property (nonatomic, readonly  , weak  ) id                target;
@property (nonatomic, readonly  , assign) SEL               selector;
@property (nonatomic, readonly  , strong) id                userInfo;
@property (nonatomic, readonly  , assign) BOOL              repeats;

@end

@implementation LTimer

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

+ (LTimer *)
timerWithTimeInterval   :(NSTimeInterval)seconds
target                  :(id            )target
selector                :(SEL           )selector
userInfo                :(id            )userInfo
{
    return [[LTimer alloc]
            initWithInterval:seconds
            target          :target
            selector        :selector
            userInfo        :userInfo
            repeats         :NO];
}

+ (LTimer *)
timerWithTimeInterval   :(NSTimeInterval)seconds
block                   :(void(^)(void) )block
{
    return [[LTimer alloc]
            initWithInterval:seconds
            block           :block
            repeats         :NO];
}

- (instancetype)
initWithInterval:(NSTimeInterval)interval
target          :(id            )target
selector        :(SEL           )selector
userInfo        :(id            )userInfo
repeats         :(BOOL          )repeats
{
    if ((self = [super init])) {
        _interval   = interval;
        _target     = target;
        _selector   = selector;
        _userInfo   = userInfo;
        _repeats    = repeats;
    }

    return self;
}

- (instancetype)
initWithInterval:(NSTimeInterval)interval
block           :(void(^)(void) )block
repeats         :(BOOL          )repeats
{
    if ((self = [super init])) {
        _interval   = interval;
        _userInfo   = [block copy];
        _repeats    = repeats;
    }

    return self;
}

- (void)dealloc
{
    [self stop];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)start
{
    [self startInRunLoop:[NSRunLoop currentRunLoop]];
}

- (void)startInRunLoop:(NSRunLoop *)runLoop
{
    [self startInRunLoop:runLoop withMode:NSRunLoopCommonModes];
}

- (void)startInRunLoop:(NSRunLoop *)runLoop withMode:(NSString *)mode
{
    [self stop];

    NSDate *date    = [NSDate dateWithTimeIntervalSinceNow:_interval];

    _NSTimer        = ((_selector != NULL)?
                       [[NSTimer alloc]
                        initWithFireDate:date
                        interval        :_interval
                        target          :_target
                        selector        :_selector
                        userInfo        :_userInfo
                        repeats         :_repeats]:
                       [[NSTimer alloc]
                        initWithFireDate:date
                        interval        :_interval
                        target          :self
                        selector        :@selector(NSTimerCallback:)
                        userInfo        :_userInfo
                        repeats         :_repeats]);

    [runLoop addTimer:_NSTimer forMode:mode];

    [self setIsRunning:YES];
}

- (void)expire
{
    id timer    = [self NSTimer];
    id userInfo = [self userInfo];

    [self stop];

    if (_selector != NULL) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [_target performSelector:_selector withObject:timer];
#pragma clang diagnostic pop
    } else {
        ((void(^)(void))userInfo)();
    }
}

- (void)stop
{
    if ([_NSTimer isValid]) {
        [self setIsRunning:NO];
        [_NSTimer invalidate];

        {_NSTimer = nil;}
    }
}


#pragma mark - NSTimer Callbacks
//*********************************************************************************************************************//

- (void)NSTimerCallback:(__unused NSTimer *)__
{
    ((void(^)(void))[_NSTimer userInfo])();
}

@end
