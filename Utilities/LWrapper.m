//
//  LWrapper.m
//  Writeability
//
//  Created by Ryan on 4/10/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//

#import "LWrapper.h"

#import "LMacros.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//





@interface LUnretainedWrapper : LWrapper

@end

@interface LWeakWrapper : LWrapper

@end

@interface LStrongWrapper : LWrapper

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LWrapper
#pragma mark -
//*********************************************************************************************************************//





@implementation LWrapper

#pragma mark - Abstract Methods
//*********************************************************************************************************************//

- (instancetype)initWithObject:(id)object __ABSTRACTMETHOD__;

- (id)reference __ABSTRACTMETHOD__;


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (id)wrapperWithUnretainedRef:(const void *)pointer
{
    id object = *((__unsafe_unretained id *)pointer);

    return [[LUnretainedWrapper alloc] initWithObject:object];
}

+ (id)wrapperWithWeakRef:(const void *)pointer
{
    id object = *((__unsafe_unretained id *)pointer);

    return [[LWeakWrapper alloc] initWithObject:object];
}

+ (id)wrapperWithStrongRef:(const void *)pointer
{
    id object = *((__unsafe_unretained id *)pointer);

    return [[LStrongWrapper alloc] initWithObject:object];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LUnretainedWrapper
#pragma mark -
//*********************************************************************************************************************//




@implementation LUnretainedWrapper
{
    __unsafe_unretained id __reference;
}

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (instancetype)initWithObject:(id)object
{
    if ((self = [super init])) {
        __reference = object;
    }

    return self;
}

- (id)reference
{
    return __reference;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LWeakWrapper
#pragma mark -
//*********************************************************************************************************************//




@implementation LWeakWrapper
{
    id __reference __weak;
}

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (instancetype)initWithObject:(id)object
{
    if ((self = [super init])) {
        __reference = object;
    }

    return self;
}

- (id)reference
{
    return __reference;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LStrongWrapper
#pragma mark -
//*********************************************************************************************************************//




@implementation LStrongWrapper
{
    __strong id __reference;
}

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (instancetype)initWithObject:(id)object
{
    if ((self = [super init])) {
        __reference = object;
    }

    return self;
}

- (id)reference
{
    return __reference;
}

@end