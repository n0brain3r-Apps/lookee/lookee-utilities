//
//  NSDate+Utilities.m
//  Utilities
//
//  Created by Ryan Blonna on 20/1/15.
//  Copyright (c) 2015 Lookee. All rights reserved.
//


#import "NSDate+Utilities.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSDate+TimeStamp
#pragma mark -
//*********************************************************************************************************************//




@implementation NSDate (TimeStamp)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)dateFromTimeStamp:(NSString *)timeStamp
{
    return [[self alloc] initWithTimeStamp:timeStamp];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithTimeStamp:(NSString *)timeStamp
{
    if ((self = [self initWithTimeIntervalSinceReferenceDate:[timeStamp doubleValue]])) {

    }

    return self;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSString *)timeStamp
{
    return [@([self timeIntervalSinceReferenceDate]) stringValue];
}

@end
