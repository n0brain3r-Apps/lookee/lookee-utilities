//
//  NSInvocation+Utilities.h
//  Utilities
//
//  Created by Ryan Blonna on 16/1/15.
//  Copyright (c) 2015 Lookee. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSInvocation (Boxed)

+ (instancetype)invocationFromBoxedRepresentation:(id)representation;


- (instancetype)initWithBoxedRepresentation:(id)representation;


- (id)boxedRepresentation;

- (id)boxedReturnValue;

@end

@interface NSMethodSignature (TypeEncoding)

- (NSString *)typeEncoding;

@end
