//
//  NSObject+Utilities.m
//  Writeability
//
//  Created by Ryan Blonna on 14/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "NSObject+Utilities.h"

#import "LMacros.h"

#import "LRuntime.h"

#import "NSData+Utilities.h"
#import "NSDate+Utilities.h"
#import "NSInvocation+Utilities.h"
#import "NSString+Utilities.h"
#import "NSValue+Utilities.h"
#import "UIColor+Utilities.h"

#import "LSerializable.h"

#import <objc/runtime.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSObject+Invocation
#pragma mark -
//*********************************************************************************************************************//




@implementation NSObject (Invocation)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSInvocation *)invocationForSelector:(SEL)selector
{
    id signature = [self.class instanceMethodSignatureForSelector:selector];
    id invocation;

    if (signature) {
        invocation = [NSInvocation invocationWithMethodSignature:signature];
        [invocation setTarget:self];
        [invocation setSelector:selector];
    }

    return invocation;
}

- (NSInvocation *)retainingInvocationForSelector:(SEL)selector
{
    NSInvocation *invocation = [self invocationForSelector:selector];

    [invocation retainArguments];

    return invocation;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSObject+Observer
#pragma mark -
//*********************************************************************************************************************//




@implementation NSObject (Observer)

#pragma mark - Static Objects
//*********************************************************************************************************************//

static const void *kNSObjectRuntimeDeallocObserverKey = &kNSObjectRuntimeDeallocObserverKey;

static const void *kNSObjectNotificationObserversKey = &kNSObjectNotificationObserversKey;


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (void)__setupObjectForDeallocObservation:(id)object
{
    if (!object) {
        return;
    }

    @synchronized (self) {
        static NSMutableSet *classes;

        if (!classes) {
            classes = [NSMutableSet new];
        }

        Class class = [object class];

        if (![classes containsObject:class]) {
            [[object class] overrideDeallocWithBlock:^(__unsafe_unretained id object, void(^dealloc)(void)) {
                @autoreleasepool {
                    for (void(^callback)() in [object __deallocCallbacks]) {
                        callback(object);
                    }
                }

                dealloc();
            }];

            [classes addObject:class];
        }
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (LKVOVerb)observerForKeyPath:(NSString *)keyPath
{
    return [self observerForKeyPaths:@[keyPath]];
}

- (LKVOVerb)observerForKeyPaths:(NSArray *)keyPaths
{
    return [self observerForKeyPaths:keyPaths options:0];
}

- (LKVOVerb)initializedObserverForKeyPath:(NSString *)keyPath
{
    return [self initializedObserverForKeyPaths:@[keyPath]];
}

- (LKVOVerb)initializedObserverForKeyPaths:(NSArray *)keyPaths
{
    return [self observerForKeyPaths:keyPaths options:NSKeyValueObservingOptionInitial];
}

- (LKVOVerb)observerForKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options
{
    return [self observerForKeyPaths:@[keyPath] options:options];
}

- (LKVOVerb)observerForKeyPaths:(NSArray *)keyPaths options:(NSKeyValueObservingOptions)options
{
    LKVOVerb verb;

    $weakify(self) {
        verb = ^(void(^block)()) {
            $strongify(self) {
                if (block) {
                    id<LKVOProtocol> observer; observer
                    =
                    [[LKVO allObservers]
                     addObserver:self
                     ofTarget   :self
                     forKeyPath :keyPaths
                     options    :NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew|options
                     block      :
                     ^(__UNUSEDARG(__weak id), __UNUSEDARG(__weak id), LKVONotification *notification) {
                         @synchronized(observer) {
                             [observer setEnabled:NO];
                             block(notification);
                             [observer setEnabled:YES];
                         }
                     }];

                    return observer;
                }
                
                return (id<LKVOProtocol>)( nil );
            }
        };
    }

    return verb;
}

- (LMapVerb)mapKeyPath:(NSString *)keyPath
{
    return [self mapKeyPaths:@[keyPath]];
}

- (LMapVerb)mapKeyPaths:(NSArray *)keyPaths
{
    LMapVerb verb;

    $weakify(self) {
        verb = ^(id object) {
            $strongify(self) {
                if (object) {
                    return
                    [self observerForKeyPaths:keyPaths
                                      options:NSKeyValueObservingOptionPrior|NSKeyValueObservingOptionInitial]
                    (^(LKVONotification *notification) {
                        if ([notification isPrior]) {
                            [object willChangeValueForKey:notification.keyPath];
                        } else {
                            [object setValue:notification.newValue forKeyPath:notification.keyPath];
                            [object didChangeValueForKey:notification.keyPath];
                        }
                    });
                }
                
                return (id)( nil );
            }
        };
    }

    return verb;
}

- (void)entangleKeyPath:(NSString *)source withKeyPath:(NSString *)destination
{
    [self entangleKeyPaths:@[source] withKeyPaths:@[destination]];
}

- (void)entangleKeyPaths:(NSArray *)sources withKeyPaths:(NSArray *)destinations
{
    $weakify(self) {
        [self observerForKeyPaths:sources options:NSKeyValueObservingOptionPrior]
        (^(LKVONotification *notification) {
            $strongify(self) {
                if ([notification isPrior]) {
                    NSUInteger index = [sources indexOfObject:notification.keyPath];

                    [self willChangeValueForKey:destinations[index]];
                } else {
                    NSUInteger index = [sources indexOfObject:notification.keyPath];

                    [self didChangeValueForKey:destinations[index]];
                }
            }
        });
    }
}

- (LNotificationVerb)observerForNotificationKey:(NSString *)notificationKey
{
    LNotificationVerb verb;

    $weakify(self) {
        verb = ^(void(^block)()) {
            $strongify(self) {
                if (block) {
                    id observation
                    =
                    [$notifier
                     addObserverForName :notificationKey
                     object             :nil
                     queue              :[NSOperationQueue currentQueue]
                     usingBlock         :^(NSNotification *notification) {
                         block(notification);
                     }];

                    return [self observerForDealloc](^{
                        [$notifier removeObserver:observation];
                    });
                }
                
                return (id)( nil );
            }
        };
    }

    return verb;
}

- (LFileMonitorVerb)observerForFileAtPath:(NSString *)filePath
{
    LFileMonitorVerb verb;

    $weakify(self) {
        verb = ^(void(^block)()) {
            $strongify(self) {
                if (block) {
                    int descriptor = open([filePath fileSystemRepresentation], O_EVTONLY);

                    if (descriptor >= 0) {
                        dispatch_queue_t    queue   = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);

                        dispatch_source_t   source  = dispatch_source_create(DISPATCH_SOURCE_TYPE_VNODE,
                                                                             descriptor,
                                                                             DISPATCH_VNODE_WRITE   |
                                                                             DISPATCH_VNODE_REVOKE  |
                                                                             DISPATCH_VNODE_DELETE  |
                                                                             DISPATCH_VNODE_RENAME,
                                                                             queue);

                        if (source) {
                            dispatch_source_set_event_handler(source, ^{
                                block(dispatch_source_get_data(source));
                            });

                            dispatch_source_set_cancel_handler(source, ^{
                                close(descriptor);
                            });

                            dispatch_resume(source);

                            return [self observerForDealloc](^{
                                dispatch_source_cancel(source);
                            });
                        } else {
                            close(descriptor);
                        }
                    } else {
                        DBGWarning(@"Ignoring non-existent file '%@'", filePath);
                    }
                }
                
                return (id)( nil );
            }
        };
    }

    return verb;
}

- (LDeallocVerb)observerForDealloc
{
    [self.class __setupObjectForDeallocObservation:self];

    LDeallocVerb verb;

    $weakify(self) {
        verb = ^(void(^block)()) {
            $strongify(self) {
                if (block) {
                    [self __addDeallocCallback:block];

                    return (id)( block );
                }
                
                return (id)( nil );
            }
        };
    }

    return verb;
}

- (void)destroyKeyPathObserver:(id)observer
{
    if ([observer conformsToProtocol:@protocol(LKVOProtocol)]) {
        [[LKVO allObservers] removeObservation:observer];
    }
}

- (void)destroyObserversForKeyPath:(NSString *)keyPath
{
    return [self destroyObserversForKeyPaths:@[ keyPath ]];
}

- (void)destroyObserversForKeyPaths:(NSArray *)keyPaths
{
    [[LKVO allObservers]
     removeObserver  :self
     ofTarget        :self
     forKeyPath      :keyPaths
     selector        :NULL];
}

- (void)destroyNotificationObserver:(id)observer
{
    if (observer) {
        void(^callback)() = observer;

        callback(self);

        [self destroyDeallocObserver:observer];
    }
}

- (void)destroyFileObserver:(id)observer
{
    if (observer) {
        void(^callback)() = observer;

        callback(self);

        [self destroyDeallocObserver:observer];
    }
}

- (void)destroyDeallocObserver:(id)observer
{
    if (observer) {
        [self __removeDeallocCallback:observer];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (NSSet *)__deallocCallbacks
{
    return [[self associatedValueForKey:kNSObjectRuntimeDeallocObserverKey] copy];
}

- (void)__addDeallocCallback:(void(^)())callback
{
    NSMutableSet *callbacks;

    @synchronized (self) {
        if (!(callbacks = [self associatedValueForKey:kNSObjectRuntimeDeallocObserverKey])) {
            callbacks = [NSMutableSet set];

            [self setAssociatedValue:callbacks forKey:kNSObjectRuntimeDeallocObserverKey];
        }
    }

    @synchronized (callbacks) {
        [callbacks addObject:callback];
    }
}

- (void)__removeDeallocCallback:(void(^)())callback
{
    NSMutableSet *callbacks;

    @synchronized (self) {
        callbacks = [self associatedValueForKey:kNSObjectRuntimeDeallocObserverKey];
    }
    
    if (callbacks) {
        @synchronized (callbacks) {
            [callbacks removeObject:callback];
        }
    }
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSObject+Runtime
#pragma mark -
//*********************************************************************************************************************//





@implementation NSObject (Runtime)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSOrderedSet *)superclasses
{
    id superclasses = nil;

    @synchronized(self) {
        static  NSMutableDictionary *classes;

        if (!classes) {
            classes = [NSMutableDictionary new];
        }

        id typeID       = $classname;

        superclasses    = classes[typeID];

        if (!superclasses) {
            superclasses = [NSMutableOrderedSet new];

            Class superclass = class_getSuperclass(self);

            while (superclass) {
                [superclasses addObject:superclass];

                superclass = class_getSuperclass(superclass);
            }

            classes[typeID] = superclasses = [superclasses copy];
        }
    }

    return superclasses;
}

+ (NSSet *)attributes
{
    id attributes = nil;

    @synchronized(self) {
        static NSMutableDictionary *classes;

        if (!classes) {
            classes = [NSMutableDictionary new];
        }

        id typeID   = $classname;

        attributes  = classes[typeID];

        if (!attributes) {
            attributes = [NSMutableSet new];

            unsigned int    count = 0;
            objc_property_t *list = class_copyPropertyList(self, &count);

            static id exclude;

            if (!exclude) {
                exclude = @Set(@"hash", @"superclass", @"description", @"debugDescription");
            }

            for (NSUInteger index = 0; index < count; index ++) {
                id attribute = @(property_getName(list[index]));

                if (![exclude containsObject:attribute]) {
                    [attributes addObject:attribute];
                }
            }

            if (count) {
                free(list);
            }

            classes[typeID] = attributes = [attributes copy];
        }
    }

    return attributes;
}

+ (NSSet *)allAttributes
{
    id attributes = nil;

    @synchronized(self) {
        static NSMutableDictionary *classes;

        if (!classes) {
            classes = [NSMutableDictionary new];
        }

        id typeID   = $classname;

        attributes  = classes[typeID];

        if (!attributes) {
            attributes  = [NSMutableSet new];

            for (Class class in $plus(@[self.class], [self.superclasses array])) {
                if ([NSObject class] != class) {
                    for (NSString *attribute in [class attributes]) {
                        [attributes addObject:attribute];
                    }
                }
            }

            classes[typeID] = attributes = [attributes copy];
        }
    }
    
    return attributes;
}

+ (NSDictionary *)infoForAttribute:(NSString *)attribute
{
    id attributes = nil;

    @synchronized(self) {
        static NSMutableDictionary *classes;

        if (!classes) {
            classes = [NSMutableDictionary new];
        }

        NSString *typeID = $classname;

        NSMutableDictionary *attributeList = classes[typeID];

        if (!attributeList) {
            classes[typeID] = attributeList = [NSMutableDictionary new];
        }

        attributes = attributeList[attribute];

        if (!attributes) {
            attributes = [NSMutableDictionary new];

            objc_property_t property    = class_getProperty(self, [attribute UTF8String]);

            if (property) {
                NSArray     *list       = [@(property_getAttributes(property)) componentsSeparatedByString:@","];

                BOOL isReadOnly = NO;

                for (NSString *attribute in list) {
                    if ([attribute hasPrefix:@"T"]) {
                        attributes[@"typeEncoding"] = [attribute substringFromIndex:1];
                    } else if ([attribute hasPrefix:@"V"]) {
                        attributes[@"variableName"] = [attribute substringFromIndex:1];
                    } else if ([attribute hasPrefix:@"G"]) {
                        attributes[@"getterName"]   = [attribute substringFromIndex:1];
                    } else if ([attribute hasPrefix:@"S"]) {
                        attributes[@"setterName"]   = [attribute substringFromIndex:1];
                    } else if ([attribute isEqualToString:@"R"]) {
                        isReadOnly = YES;
                    }
                }

                if (!attributes[@"getterName"]) {
                    attributes[@"getterName"] = [attribute copy];
                }

                if (!isReadOnly && !attributes[@"setterName"]) {
                    attributes[@"setterName"] = @Join(@"set", @UpperCamelCase(attribute));
                }
            }

            attributeList[attribute] = attributes = [attributes copy];
        }
    }

    return attributes;
}

+ (NSString *)encodingForAttribute:(NSString *)attribute
{
    return [self infoForAttribute:attribute][@"typeEncoding"];
}

+ (BOOL)hasSetterForAttribute:(NSString *)attribute
{
    return !![self infoForAttribute:attribute][@"setterName"];
}

+ (BOOL)hasMutableAttribute:(NSString *)attribute
{
    return [[self encodingForAttribute:attribute] hasPrefix:@"NSMutable"];
}

+ (NSString *)encodingForMethodSelector:(SEL)selector
{
    return @(method_getTypeEncoding(class_getInstanceMethod(self, selector)));
}

+ (void)forEachMethodSelector:(void(^)(SEL, BOOL *))block
{
    BOOL stop = NO;

    for (Class class = $class; class != nil; class = class_getSuperclass(class)) {
        Method *methodList = class_copyMethodList(class, NULL);

        if (methodList) {
            for (Method *reference = methodList; *reference != NULL; reference ++) {
                block(method_getName(*reference), &stop);

                if (stop) {
                    break;
                }
            }

            free(methodList);
        }
    }
}

+ (Class)declareSubclassNamed:(NSString *)name
{
    DBGParameterAssert(name != nil);

    Class class = objc_allocateClassPair(self, [name UTF8String], 0);

    objc_registerClassPair(class);

    return class;
}

+ (void)addPropertyWithName:(NSString *)name typeEncoding:(NSString *)typeEncoding
{
    [self addPropertyWithName:name typeEncoding:typeEncoding shouldCopy:NO];
}

+ (void)
addPropertyWithName :(NSString *)name
typeEncoding        :(NSString *)typeEncoding
shouldCopy          :(BOOL      )shouldCopy
{
    [self addPropertyWithName:name typeEncoding:typeEncoding shouldCopy:shouldCopy isAtomic:NO];
}

+ (void)
addPropertyWithName :(NSString *)name
typeEncoding        :(NSString *)typeEncoding
shouldCopy          :(BOOL      )shouldCopy
isAtomic            :(BOOL      )isAtomic
{
    [self addPropertyWithName:name typeEncoding:typeEncoding shouldCopy:shouldCopy isAtomic:isAtomic getterName:nil];
}

+ (void)
addPropertyWithName :(NSString *)name
typeEncoding        :(NSString *)typeEncoding
shouldCopy          :(BOOL      )shouldCopy
isAtomic            :(BOOL      )isAtomic
getterName          :(NSString *)getterName
{
    [self
     addPropertyWithName:name
     typeEncoding       :typeEncoding
     shouldCopy         :shouldCopy
     isAtomic           :isAtomic
     getterName         :getterName
     setterName         :nil];
}

+ (void)
addPropertyWithName :(NSString *)name
typeEncoding        :(NSString *)typeEncoding
shouldCopy          :(BOOL      )shouldCopy
isAtomic            :(BOOL      )isAtomic
getterName          :(NSString *)getterName
setterName          :(NSString *)setterName
{
    NSUInteger                  attributeCount  = 0;

    objc_property_attribute_t   attributes[3]   = {0};

    {
        objc_property_attribute_t type = {
            .name   = "T",
            .value  = [typeEncoding UTF8String]
        };

        attributes[attributeCount ++] = type;
    }
    {
        objc_property_attribute_t customGetter;

        if (getterName) {
            customGetter.name   = "G";
            customGetter.value  = [getterName UTF8String];

            attributes[attributeCount ++] = customGetter;
        }
    }
    {
        objc_property_attribute_t customSetter;

        if (setterName) {
            customSetter.name   = "S";
            customSetter.value  = [setterName UTF8String];

            attributes[attributeCount ++] = customSetter;
        }
    }

    class_addProperty(self, [name UTF8String], attributes, attributeCount);

    if (!getterName) {
        getterName = name;
    }

    if (!setterName) {
        setterName = @Join(@"set", @UpperCamelCase(name));
    }

    SEL getter = NSSelectorFromString(getterName);
    SEL setter = NSSelectorFromString(setterName);

    LRunTimeAssociationPolicy policy;

    if ([[typeEncoding substringToIndex:1] isEqualToString:@"@"]) {
        policy = kLRunTimeAssociationRetainNonatomic;
    } else {
        policy = kLRunTimeAssociationAssign;
    }

    if (policy != kLRunTimeAssociationAssign) {
        if (shouldCopy) {
            if (isAtomic) {
                policy = kLRunTimeAssociationCopy;
            } else {
                policy = kLRunTimeAssociationCopyNonatomic;
            }
        } else {
            if (isAtomic) {
                policy = kLRunTimeAssociationRetain;
            }
        }
    }

    [self addMethodWithSelector:getter withBlock:^(id self) {
        return objc_getAssociatedObject(self, getter);
    }];

    [self addMethodWithSelector:setter withBlock:^(id self, id value) {
        objc_setAssociatedObject(self, getter, value, policy);
    }];
}

+ (void)addMethodSelector:(SEL)selector withImplementation:(void *)implementation andEncoding:(NSString *)encoding
{
    class_addMethod(self, selector, implementation, [encoding UTF8String]);
}

+ (void)addMethodWithSelector:(SEL)selector withBlock:(id)block;
{
    if (![self instancesRespondToSelector:selector]) {
        const char *encoding;

        if (![self instancesRespondToSelector:selector]) {
            encoding = BLKEncoding(block);
        } else {
            encoding = method_getTypeEncoding(class_getInstanceMethod(self, selector));
        }

        class_addMethod(self, selector, imp_implementationWithBlock(block), encoding);
    }
}

+ (void)addMethodSelector:(SEL)selector fromType:(Class)class
{
    if ([class instanceMethodForSelector:selector]) {
        Method      method          = class_getInstanceMethod(class, selector);

        const char  *encoding       = method_getTypeEncoding(method);

        IMP         implementation  = method_getImplementation(method);

        [self addMethodSelector:selector withImplementation:implementation andEncoding:@(encoding)];
    }
}

+ (void)overrideDeallocWithBlock:(void(^)(__unsafe_unretained id object, void(^dealloc)(void)))block
{
    SEL     selector        = NSSelectorFromString(@"dealloc");
    Method  method          = class_getInstanceMethod(self, selector);
    IMP     implementation  = method_getImplementation(method);

    class_replaceMethod
    (self,
     selector,
     imp_implementationWithBlock(^(void *object) {
        block(Objectify(object), ^{
            ((void(*)(void *, SEL))implementation)(object, selector);
        });
    }),
     method_getTypeEncoding(method));
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (id)associatedObject
{
    return [self associatedValueForKey:@selector(associatedObject)];
}

- (void)setAssociatedObject:(id)associatedObject
{
    [self setAssociatedValue:associatedObject forKey:@selector(associatedObject)];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (id)associatedValueForKey:(const void *)key
{
    return objc_getAssociatedObject(self, key);
}

- (void)setAssociatedValue:(id)value forKey:(const void *)key
{
    objc_setAssociatedObject(self, key, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSDictionary *)propertyDump
{
    NSMutableDictionary *dump = [NSMutableDictionary new];

    for (NSString *attribute in [$class allAttributes]) {
        NSString *encoding  = [$class encodingForAttribute:attribute];
        NSString *key       = [NSString stringWithFormat:@"(%@) %@", encoding, attribute];

        dump[key] = [self valueForKey:attribute]?:[NSNull null];
    }

    return [dump copy];
}

@end





//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSObject+Serialization
#pragma mark -
//*********************************************************************************************************************//





@implementation NSObject (Serialization)

#pragma mark - Static Functions
//*********************************************************************************************************************//

static id __deserialize(id serialized, BOOL mutable, id(^verb)(Class class, id parameter))
{
    if (!serialized) {
        return nil;
    }

    id label, value;
    {
        switch ([serialized count]) {
            case 2: {
                label = serialized[0];
                value = serialized[1];
                break;
            }
            case 1: {
                value = serialized[0];
                label = getTypeString(value);
                break;
            }

            default: return nil;
        }
    }

    id $sub(deserializeClass, id label) {
        return NSClassFromString(label);
    };

    id $sub(deserializeBasicObject, id instance) {
        if (mutable && [instance respondsToSelector:@selector(mutableCopy)]) {
            return [instance mutableCopy];
        }

        return instance;
    };

    id $sub(deserializeLinearCollection, NSArray *collection) {
        id instance = [[NSClassFromString(label) new] mutableCopy];

        for (id member in collection) {
            [instance addObject:deserialize(member, verb)];
        }

        return (!mutable? instance: [instance copy]);
    };

    id $sub(deserializeMappingCollection, NSDictionary *collection) {
        id instance = [[NSClassFromString(label) new] mutableCopy];

        for (id code in collection) {
            id key      = deserialize([code objectRepresentation], verb);
            id value    = deserialize([collection objectForKey:code], verb);

            [instance setObject:value forKey:key];
        }

        return (!mutable? instance: [instance copy]);
    };

    id $sub(deserializeObject, id descriptor) {
        Class class = NSClassFromString(label);

        id instance;

        if (verb) {
            id serialized = descriptor[@"@"];

            if (serialized) {
                instance = verb(class, deserialize(serialized, verb));
            }
        }

        if (!instance) {
            instance = [class new];
        }

        if ([instance conformsToProtocol:@protocol(LSerializable)]) {
            for (id key in $minus(descriptor, @[@"@"])) {
                id value = __deserialize(descriptor[key], [class hasMutableAttribute:key], verb);

                if (!value) {
                    DBGWarning(@"Failed to deserialize %@.%@", instance, key);
                    continue;
                }

                [instance setValue:value forKey:key];
            }
        }

        return instance;
    };

    $switch(label) {
            $case(@"Class"):
                return deserializeClass(value);

            $case(@"NSNumber", @"NSNull", @"NSString"):
                return deserializeBasicObject(value);

            $case(@"NSArray", @"NSOrderedSet", @"NSSet"):
                return deserializeLinearCollection(value);

            $case(@"NSDictionary"):
                return deserializeMappingCollection(value);
            
            $default:
                return deserializeObject(value);
    }
}

id __serialize(id object, NSSet *attributes, id(^verb)(id object))
{
#define willInferLabel (label = nil)

    if (!object) {
        return nil;
    }

    __block id label = getTypeString(object);

    id $sub(serializeClass, Class class) {
        return NSStringFromClass(class);
    };

    id $sub(serializeBasicObject, id value) {
        if ([value respondsToSelector:@selector(copy)]) {
            return [value copy];
        }

        return value;
    };

    id $sub(serializeLinearCollection, id collection) {
        NSMutableArray *value = [NSMutableArray new];

        for (id member in collection) {
            [value addObject:serialize(member, verb)];
        }

        return [value copy];
    };

    id $sub(serializeMappingCollection, id collection) {
        NSMutableDictionary *value = [NSMutableDictionary new];

        for (id key in collection) {
            id entry    = serialize(key, verb);
            id member   = serialize([collection objectForKey:key], verb);

            [value setObject:member forKey:[NSString stringFromObjectRepresentation:entry]];
        }

        return [value copy];
    };

    id $sub(serializeObject, id instance) {
        id value = [NSMutableDictionary new];

        if (verb) {
            id classname, object;
            $tup(classname, object) = verb(instance);

            if (object) {
                label       = classname;
                value[@"@"] = serialize(object, verb);
            }
        }

        if ([object conformsToProtocol:@protocol(LSerializable)]) {
            for (NSString *key in attributes) {
                id member = [instance valueForKey:key];

                if (member) {
                    [value setObject:serialize(member, verb) forKey:key];
                }
            }
        }

        return value;
    };

    id value;

    $switch(label) {
            $case(@"Class"):
                value = serializeClass(object);

            $case(@"NSNumber", @"NSNull", @"NSString"):
                value = serializeBasicObject(object), willInferLabel;

            $case(@"NSArray"):
                value = serializeLinearCollection(object), willInferLabel;

            $case(@"NSDictionary"):
                value = serializeMappingCollection(object), willInferLabel;

            $case(@"NSOrderedSet", @"NSSet"):
                value = serializeLinearCollection(object);

            $default:
                value = serializeObject(object);
    }
    
    if (!value) {
        return @[];
    }

    if (!label) {
        return @[value];
    }
    
    return @[label, value];

#undef willInferLabel
}

NSString *getTypeString(id object)
{
    if (IsClassObject(object)) {
        return @("Class");
    }

    for (Class class in
         @[
           [NSNull class],
           [NSNumber class],
           [NSString class],
           [NSArray class],
           [NSDictionary class],
           [NSOrderedSet class],
           [NSSet class]
           ]) {
        if ([object isKindOfClass:class]) {
            return NSStringFromClass(class);
        }
    }

    return NSStringFromClass([object class]);
}


#pragma mark - Global Functions
//*********************************************************************************************************************//

id deserialize(id serialized, id(^verb)(Class class, id parameter))
{
    return __deserialize(serialized, NO, verb);
}

id serialize(id object, id(^verb)(id object))
{
    NSMutableSet *attributes = [NSMutableSet new];

    for (NSString *attribute in [[object class] allAttributes]) {
        if ([[object class] hasSetterForAttribute:attribute]) {
            [attributes addObject:attribute];
        }
    }

    return __serialize(object, attributes, verb);
}


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (id)newFromJSONString:(NSString *)JSONString
{
    return [self newFromJSONString:JSONString deserializer:nil];
}

+ (id)newFromJSONString:(NSString *)JSONString deserializer:(id(^)(Class, id))verb
{
    if (!JSONString) {
        return nil;
    }

    id objectVerb = ^(Class class, id parameter) {
        id instance;

        if ([class isSubclassOfClass:[NSData class]]) {
            instance = [NSData dataFromBase64Representation:parameter];
        } else if ([class isSubclassOfClass:[NSDate class]]) {
            instance = [NSDate dateFromTimeStamp:parameter];
        } else if ([class isSubclassOfClass:[NSInvocation class]]) {
            instance = [NSInvocation invocationFromBoxedRepresentation:parameter];
        } else if ([class isSubclassOfClass:[NSValue class]]) {
            instance = [NSValue valueFromBoxedRepresentation:parameter];
        } else if ([class isSubclassOfClass:[UIColor class]]) {
            instance = [UIColor colorFromHexString:parameter];
        }

        if (!instance && verb) {
            instance = verb(class, parameter);
        }

        return instance;
    };

    return deserialize([JSONString objectRepresentation], objectVerb);
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSString *)convertToJSONString
{
    return [self convertToJSONString:nil];
}

- (NSString *)convertToJSONString:(id(^)(id))verb
{
    id objectVerb = ^(id object) {
        id value;

        Class class = [object class];

        if ([class isSubclassOfClass:[NSData class]]) {
            value = [object base64Representation];
            class = [NSData class];
        } else if ([class isSubclassOfClass:[NSDate class]]) {
            value = [object timeStamp];
            class = [NSDate class];
        } else if ([class isSubclassOfClass:[NSInvocation class]]) {
            value = [object boxedRepresentation];
            class = [NSInvocation class];
        } else if ([class isSubclassOfClass:[NSValue class]]) {
            value = [object boxedRepresentation];
            class = [NSValue class];
        } else if ([class isSubclassOfClass:[UIColor class]]) {
            value = [object hexString];
            class = [UIColor class];
        }

        if (!value && verb) {
            value = verb(object);
        }

        if (!value) {
            return @[class.description];
        }

        return @[class.description, value];
    };

    return [NSString stringFromObjectRepresentation:serialize(self, objectVerb)];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LMethodDescriptor

@end
