//
//  NSOperationQueue+Utilities.h
//  Writeability
//
//  Created by Ryan on 8/24/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSOperationQueue (Utilities)

- (void)addSerialOperation:(NSOperation *)operation;
- (void)addSerialOperationWithBlock:(void(^)(void))block;

- (void)addSynchronousOperation:(NSOperation *)operation;
- (void)addSynchronousOperationWithBlock:(void(^)(void))block;

@end
