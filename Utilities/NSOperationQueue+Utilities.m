//
//  NSOperationQueue+Utilities.m
//  Writeability
//
//  Created by Ryan on 8/24/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "NSOperationQueue+Utilities.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSOperationQueue+Utilities
#pragma mark -
//*********************************************************************************************************************//




@implementation NSOperationQueue (Utilities)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)addSerialOperation:(NSOperation *)operation
{
    if ([NSOperationQueue currentQueue] == self) {
        [operation start];
    } else {
        [self addOperation:operation];
    }
}

- (void)addSerialOperationWithBlock:(void(^)(void))block
{
    [self addSerialOperation:[NSBlockOperation blockOperationWithBlock:block]];
}

- (void)addSynchronousOperation:(NSOperation *)operation
{
    if ([NSOperationQueue currentQueue] == self) {
        [operation start];
    } else {
        [self addOperation:operation];
        [self waitUntilAllOperationsAreFinished];
    }
}

- (void)addSynchronousOperationWithBlock:(void(^)(void))block
{
    [self addSynchronousOperation:[NSBlockOperation blockOperationWithBlock:block]];
}

@end
