//
//  NSString+Utilities.h
//  Writeability
//
//  Created by Ryan on 8/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


// TODO: Somehow integrate DiffMatchPatch


@interface NSString (Convert)

- (NSUInteger)convertToBase:(NSUInteger)base;

@end

@interface NSString (Representation)

+ (instancetype)stringFromReadableRepresentation:(NSString *)representation;
+ (instancetype)stringFromCompactRepresentation:(NSString *)representation;
+ (instancetype)stringFromObjectRepresentation:(id)representation;


- (NSString *)readableRepresentation;
- (NSString *)compactRepresentation;
- (id)objectRepresentation;

@end

@interface NSString (Substring)

- (BOOL)hasSubstring:(NSString *)substring;

- (NSString *)stringByDeletingSubstring:(NSString *)substring;

@end

@interface NSString (Match)

- (BOOL)isMatch:(NSString *)pattern;

- (NSArray *)match:(NSString *)pattern;

@end
