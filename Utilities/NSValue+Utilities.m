//
//  NSValue+Utilities.m
//  Utilities
//
//  Created by Ryan Blonna on 15/1/15.
//  Copyright (c) 2015 Lookee. All rights reserved.
//


#import "NSValue+Utilities.h"

#import "NSString+Utilities.h"
#import "NSData+Utilities.h"





//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSValue+Boxed
#pragma mark -
//*********************************************************************************************************************//





@implementation NSValue (Boxed)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)valueFromBoxedRepresentation:(id)representation
{
    return [[self alloc] initWithBoxedRepresentation:representation];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithBoxedRepresentation:(id)representation
{
    const char  *objCType   = [[representation firstObject] UTF8String];
    const void  *value      = [[NSData dataFromBase64Representation:representation[1]] bytes];

    if ((self = [self initWithBytes:value objCType:objCType])) {

    }

    return self;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (id)boxedRepresentation
{
    NSString *objCTypeString    = [[NSString alloc] initWithUTF8String:self.objCType];
    NSString *valueString       = [[NSData dataWithValue:self] base64Representation];

    return @[objCTypeString, valueString];
}

@end
