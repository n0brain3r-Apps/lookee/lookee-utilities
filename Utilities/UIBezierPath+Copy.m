//
//  UIBezierPath+Copy.m
//  Writeability
//
//  Created by Ryan on 11/14/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "UIBezierPath+Copy.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark UIBezierPath+Copy
#pragma mark -
//*********************************************************************************************************************//





@implementation UIBezierPath (Copy)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (UIBezierPath *)bezierPathCopyByStrokingRect:(CGRect)rect lineWidth:(CGFloat)lineWidth
{
    UIBezierPath    *path   = [UIBezierPath bezierPathWithRect:rect];

    CGPathRef       pathRef = CGPathCreateCopyByStrokingPath(path.CGPath,
                                                             NULL,
                                                             lineWidth,
                                                             kCGLineCapButt,
                                                             kCGLineJoinMiter,
                                                             1.);

    UIBezierPath    *copy   = [UIBezierPath bezierPathWithCGPath:pathRef];

    CGPathRelease(pathRef);

    return copy;
}

@end
