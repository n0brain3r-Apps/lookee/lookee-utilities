//
//  UIColor+Utilities.h
//  Writeability
//
//  Created by Ryan on 8/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIColor (Compare)

- (BOOL)isEqualToColor:(UIColor *)color;

- (BOOL)isLight;

@end

@interface UIColor (Components)

- (void)getComponents:(float *)rgba;

- (float)red;
- (float)green;
- (float)blue;
- (float)alpha;

@end

@interface UIColor (GL)

- (void)bindGLClearColor;

@end

@interface UIColor (HexString)

+ (instancetype)colorFromHexString:(NSString *)hexString;


- (instancetype)initWithHexString:(NSString *)hexString;


- (NSString *)hexString;

@end
