//
//  UIScrollView+Scroll.m
//  Writeability
//
//  Created by Ryan on 12/19/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "UIScrollView+Scroll.h"


@implementation UIScrollView (Scroll)

- (void)scrollToTop
{
    [self scrollToTopAnimated:NO];
}

- (void)scrollToTopAnimated:(BOOL)animated
{
    [self
     setContentOffset   :((CGPoint) {
        +self.contentOffset.x,
        -self.contentInset.top
     })
     animated           :animated];
}

@end
